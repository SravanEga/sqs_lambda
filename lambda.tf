data "archive_file" "example_lambda" {
  type        = "zip"
  source_file = "${path.module}/example_lambda.js"
  output_path = "${path.module}/example_lambda.js.zip"
}

data "aws_vpc" "aws-vpc" {
  # Name = "Gitlab-artif-VPC"
  filter {
    name   = "tag:Name"
    values = ["Gitlab-*"]
  }
  # filter {
  #   name   = "tag-key"
  #   values = ["Name"]
  # }
  # tags = {
  #   Name = "Gitlab-artif-VPC"
  # }
}

#data "aws_availability_zone" {}

data "aws_subnet_ids" "pub_sub" {
  #count = 1
  vpc_id = data.aws_vpc.aws-vpc.id
  #vpc_id = "vpc-08a2af44024e53be4"
  tags = {
    #type = list(string)
    Name = "gitlab-artif-shared-pubsubnet-*" #*${var.availability_zone}*"
  }
}

data "aws_security_group" "manual" {
  name = "harness-sqs-lambda-manual"
}

resource "aws_lambda_function" "example_lambda" {
  function_name = "harness_lambda_sqs"
  handler       = "example_lambda.handler"
  role          = aws_iam_role.example_lambda.arn
  runtime       = "nodejs14.x"

  filename         = data.archive_file.example_lambda.output_path
  source_code_hash = data.archive_file.example_lambda.output_base64sha256

  timeout         = 30
  memory_size     = 128
 # vpc_subnet_ids         = data.aws_subnet_id.pub_sub.*.id
 # vpc_security_group_ids = [data.aws_security_group.manual.id]
 # attach_network_policy  = true
  count = "1"
  vpc_config {
    subnet_ids = element(data.aws_subnet_ids.pub_sub.*.ids, count.index)
    security_group_ids = [data.aws_security_group.manual.id]
}

}
